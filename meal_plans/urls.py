from django.urls import path

from django.contrib.auth import views as auth_views

from meal_plans.views import (
    MealPlanListView,
    MealPlanCreateView,
    MealPlanDetailView,
    MealPlanEditView,
    MealPlanDeleteView,
)

urlpatterns = [
    path("", MealPlanListView.as_view(), name="meal_plan_list"),
    path("new/", MealPlanCreateView.as_view(), name="meal_plan_create"),
    path("<int:pk>/", MealPlanDetailView.as_view(), name="meal_plan_detail"),
    path("<int:pk>/edit/", MealPlanEditView.as_view(), name="meal_plan_edit"),
    path(
        "<int:pk>/delete/",
        MealPlanDeleteView.as_view(),
        name="meal_plan_delete",
    ),
]
