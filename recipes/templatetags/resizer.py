from django import template

register = template.Library()


def resize_to(ingredient, target):
    numberofservings = ingredient.recipe.servings
    amount = ingredient.amount

    if numberofservings is not None and target is not None:
        ratio = int(target) / int(numberofservings)
        newamount = ratio * int(amount)
        return newamount

    else:
        return amount


register.filter(resize_to)
